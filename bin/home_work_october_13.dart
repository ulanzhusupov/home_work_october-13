import 'dart:io';

import 'package:dio/dio.dart';
import 'package:translator/translator.dart';

void main(List<String> arguments) {
  print(
      "Выберите язык факта (введите только цифры):\n1. Кыргызский   2. Русский   3. Испанский\n4. Французский  5. Английский\n");
  String lang = stdin.readLineSync() ?? '1';
  switch (lang) {
    case "1":
      translatorFunc("ky");
    case "2":
      translatorFunc("ru");
    case "3":
      translatorFunc("es");
    case "4":
      translatorFunc("fr");
    case "5":
      translatorFunc("en");
    default:
      break;
  }
}

Future<void> translatorFunc(String language) async {
  String continueFacts;
  List<String> likedFacts = [];

  do {
    try {
      print("Загрузка факта...\n");
      String input = await getFact();
      String translatedFact = await translateFact(input, language);
      continueFacts = printFactAndChooseAction(translatedFact);

      if (continueFacts == "1") {
        likedFacts.add(translatedFact);
      } else if (continueFacts == "3") {
        printLikedFacts(likedFacts);
      } else if (continueFacts == "2") {
        continue;
      }
    } catch (error) {
      print(error);
      if (error is DioException) {
        print("Ошибка в запросе");
      }
      print("Язык не поддерживается");
      continueFacts = '4';
    }
  } while (continueFacts != '4');
}

Future<String> getFact() async {
  final dynamic response =
      await Dio().get("https://catfact.ninja/fact").then((value) => value);

  String fact = response.data['fact'];

  return fact;
}

Future<String> translateFact(String input, String language) async {
  final translator = GoogleTranslator();
  String translatedText = await translator
      .translate(input, to: language)
      .then((value) => value.text);

  return translatedText;
}

String printFactAndChooseAction(String fact) {
  print("$fact\n");
  print(
      "Выберите дальнейшее действие (введите только цифры):\n1. Понравился   2. Далее   3. Показать список понравившихся фактов\n4. Закончить\n");
  String chooseAction = stdin.readLineSync() ?? '';

  return chooseAction;
}

void printLikedFacts(List<String> facts) {
  if (facts.isNotEmpty) {
    for (var element in facts) {
      print("$element ");
    }
    print("\nОчистить список? (y/n)");
    String eraseOrNot = stdin.readLineSync() ?? "";
    if (eraseOrNot == "y") {
      facts.clear();
    }
  } else {
    print("Список пуст\n");
  }
}
